#include <iostream>

template <class T>
int Compare(T a, T b)
{
	if (b > a)
	{
		return 1;
	}
	else if (a > b)
	{
		return -1;
	}
	return 0;
}
/*
int main()
{
	std::cout << Compare('c', 'b');
	return 0;
}*/