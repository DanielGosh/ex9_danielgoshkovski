#include <iostream>
template <class T>
void bubbleSort(T arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		// Last i elements are already in place  
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				T temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
				
		}
	}
		
}


/*
int main()
{
	char arr[] = {'a', 'c', 'f', 'b', 'e'};
	for (int i = 0; i < 5; i++)
	{
		std::cout << arr[i];
	}
	bubbleSort(arr, 5);
	std::cout << "\n";
	for (int i = 0;i < 5;i++)
	{
		std::cout << arr[i];
	}
}*/