#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = NULL;
	this->_right = NULL;
}

BSNode::BSNode(const BSNode & other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
}

void BSNode::insert(std::string value)
{
	this->_data = value;
}

bool BSNode::isLeaf() const
{
	bool leafBool = false;
	if ((this->_right == NULL) && (this->_left == NULL))
	{
		leafBool = true;
	}
	return leafBool;
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{

	if (this->_data == val)
	{
		return true;
	}
	else
	{ 
		if( search(val) == true)
		{
			return true;
		}
		else if( search(val) == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

int BSNode::getHeight() const
{
	int maxHeight;
	int high = 0;
	if (this != NULL)
	{
		int lHeight = (this->_left)->getHeight();
		int rHeight = (this->_right)->getHeight();
		if (lHeight> rHeight)
		{
			maxHeight = lHeight;
		}
		else
		{
			maxHeight = rHeight;
		}
		high = maxHeight + 1;
	}
	return high;
}

int BSNode::getDepth(BSNode* root) const
{
	BSNode* node = root;
	if (node == NULL)  
        return 0;  
    else
    {  
        /* compute the depth of each subtree */
        int lDepth = getDepth(node->_left);
        int rDepth = getDepth(node->_right);
      
        /* use the larger one */
        if (lDepth > rDepth)  
            return(lDepth + 1);  
        else return(rDepth + 1);  
    } 
}
